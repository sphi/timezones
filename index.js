
function hsvValue2rgbValue(h, s, v) {
  const a = (Math.sin(h * Math.PI * 2) + 1) * 0.5;
  const b = a * s + ((1 - s) / 2);
  const c = b * v;
  return Math.min(Math.max(c, 0), 1);
}

function hsva2rgba(h, s, v, a) {
  s = Math.min(Math.max(s, 0), 1);
  v = Math.min(Math.max(v, 0), 1);
  a = Math.min(Math.max(a, 0), 1);
  const r = hsvValue2rgbValue(h + 0.00000, s, v) * 255;
  const g = hsvValue2rgbValue(h + 0.33333, s, v) * 255;
  const b = hsvValue2rgbValue(h + 0.66666, s, v) * 255;
  return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + a + ')'
}

function formatTime(time, tz) {
  const hour = (time + tz + 24) % 24;
  if (tz < 0) {
    return (((hour + 11) % 12) + 1) + (hour >= 12 ? 'p' : 'a');
  } else {
    return (((hour + 23) % 24) + 1) + '';
  }
}

const morningColor = '#E0A03060'
const eveningColor = '#40FFFF60'
const nightColor = '#10208060'

function colorForTime(time, a) {
  const day = Math.abs((time + 11) % 24 - 12) / 6;
  const morn = Math.abs((time + 7) % 24 - 12) / 6;
  return hsva2rgba(morn / 4 - 0.4, 0.6, Math.max(day * 1.6 - 0.6, 0.1), a);
}

function setupMainTable() {
  const table = document.getElementById('main-table');
  {
    const row = document.createElement('tr');
    table.appendChild(row);
    for (let tz = -10; tz <= 12; tz += 2) {
      const item = document.createElement('th');
      item.className = 'time-item time-label';
      item.style.backgroundColor = hsva2rgba((tz + 8) / 24, 0.8, 0.7, 0.8);
      item.textContent = tz == 0 ? 'UTC' : ((tz > 0 ? '+' : '') + tz);
      row.appendChild(item);
    }
  }
  for (let time = 0; time <= 23; time += 3) {
    const row = document.createElement('tr');
    table.appendChild(row);
    for (let tz = -10; tz <= 12; tz += 2) {
      const item = document.createElement('th');
      item.className = 'time-item';
      item.style.backgroundColor = hsva2rgba((tz + 8) / 24, 0.7, 0.5, 0.6);
      item.textContent = formatTime(time, tz);
      row.appendChild(item);
    }
  }
}

setupMainTable();
